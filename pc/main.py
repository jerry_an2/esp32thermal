import asyncio
from bleak import BleakScanner, BleakClient
import struct

SERVICE_UUID        = "bf7a377e-655b-4b77-ab25-a75d7da4aa27"
CHARACTERISTIC_UUID = "421c2af9-e4dc-4618-a5f7-0fa81bfc688b"

async def main():
    ret_q = []
    stop_event = asyncio.Event()

    def on_discover(device, data):
        ret_q.append(device.address)
        stop_event.set()

    # async with BleakScanner(detection_callback=on_discover, service_uuids=[SERVICE_UUID]) as scanner:
    async with BleakScanner(detection_callback=on_discover, service_uuids=[SERVICE_UUID]) as scanner:
        await stop_event.wait()
    
    address = ret_q.pop(0)
    print(address)

    client = BleakClient(address)
    try:
        await client.connect()
        while True:
            val = await client.read_gatt_char(CHARACTERISTIC_UUID)
            timestamp, temperature = struct.unpack("if", val)
            print(f"{timestamp}: {temperature}C")
            await asyncio.sleep(2)
    except Exception as e:
        print(e)
    finally:
        await client.disconnect()

if __name__ == "__main__":
    asyncio.run(main())