#include <Arduino.h>
#include <BLEDevice.h>
#include <BLEUtils.h>
#include <BLEServer.h>
#include <ctime>

// See the following for generating UUIDs:
// https://www.uuidgenerator.net/

#define SERVICE_UUID        "bf7a377e-655b-4b77-ab25-a75d7da4aa27"
#define CHARACTERISTIC_UUID "421c2af9-e4dc-4618-a5f7-0fa81bfc688b"

class MyCallbacks: public BLECharacteristicCallbacks {
    void onWrite(BLECharacteristic *pCharacteristic) {
      std::string value = pCharacteristic->getValue();

      if (value.length() > 0) {
        Serial.println("*********");
        Serial.print("New value: ");
        for (int i = 0; i < value.length(); i++)
          Serial.print(value[i]);

        Serial.println();
        Serial.println("*********");
      }
    }

    void onDisconnect(BLEServer* pServer) {
      Serial.print("Device disconnected");
      BLEAdvertising *pAdvertising = pServer->getAdvertising();
      pAdvertising->addServiceUUID(SERVICE_UUID);
      pAdvertising->start();
    }
};

BLECharacteristic *pCharacteristic;

void setup() {
  Serial.begin(9600);

  BLEDevice::init("MyESP32");
  BLEServer *pServer = BLEDevice::createServer();

  BLEService *pService = pServer->createService(SERVICE_UUID);

  pCharacteristic = pService->createCharacteristic(
                                         CHARACTERISTIC_UUID,
                                         BLECharacteristic::PROPERTY_READ |
                                         BLECharacteristic::PROPERTY_WRITE
                                       );

  pCharacteristic->setCallbacks(new MyCallbacks());

  pCharacteristic->setValue("Hello World");
  pService->start();

  BLEAdvertising *pAdvertising = pServer->getAdvertising();
  pAdvertising->addServiceUUID(SERVICE_UUID);
  pAdvertising->start();
}

void loop() {
  // put your main code here, to run repeatedly:
  int32_t datapack[2];
  float* val_ptr = (float*)&datapack[1];
  float temp_c;
  temp_c = random(35,42);
  datapack[0] = time(nullptr);
  *val_ptr = temp_c;
  pCharacteristic->setValue((uint8_t*)datapack, sizeof(datapack));
  delay(2000);
}